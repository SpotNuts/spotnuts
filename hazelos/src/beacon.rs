use esp_backtrace as _;

use hal::{prelude::*, rng::Rng, sha::Sha};

use libjayjeer::Jayjeer;

use crate::state;
use crate::Error;

pub(crate) const BEACON_SECS_MIN: u8 = 5;
pub(crate) const BEACON_SECS_MAX: u8 = 12;
pub(crate) const BEACONS_BEFORE_ROLLOVER: u8 = 10;

// FIXME Workaround for https://github.com/esp-rs/esp-hal/issues/950
#[ram(rtc_fast, uninitialized)]
static mut _RTC_MEM_PROTECTIVE_BYTES: [u8; 8] = [0u8; 8];
#[ram(rtc_fast, uninitialized)]
static mut BEACON_PUBLIC_KEY: [u8; 32] = [0u8; 32];
#[ram(rtc_fast, uninitialized)]
static mut BEACON_COUNT: u8 = 0;

fn reset_beacon_data(jayjeer: &mut Jayjeer) {
    log::debug!("Resetting beacon data in memory from jayjeer.");
    unsafe {
        BEACON_PUBLIC_KEY = jayjeer.get_current_public_key().to_bytes();
        BEACON_COUNT = 0;
        log::info!("Beacon public key: {:02x?}", BEACON_PUBLIC_KEY);
        log::debug!("Beacon counter reset to 0.");
    };
}

pub(crate) fn init(rng: &mut Rng, sha: &mut Sha) -> Result<[u8; 32], Error> {
    log::info!("Initializing after power up.");

    let mut jayjeer = match state::NutState::read_from_flash(sha) {
        Err(Error::InvalidFlashData) => {
            log::warn!("Invalid checksum in flash data. Resetting keys.");

            // FIXME enable bluetooth before doingthis for true RNG
            let mut bytes = [0u8; 32];
            if rng.read(&mut bytes).is_err() {
                log::error!("Could not read 32 random bytes. Resetting device!");
                return Err(Error::BoardError);
            }
            let jj = Jayjeer::from(bytes);

            let mut state = state::NutState::from(&jj);
            state.write_to_flash(sha);
            jj
        }
        Err(e) => return Err(e),
        Ok(state) => {
            log::info!("Loaded keys from flash.");
            let mut jj: Jayjeer = (&state).into();
            jj.rollover();
            jj
        }
    };
    reset_beacon_data(&mut jayjeer);
    Ok(jayjeer.get_current_public_key().to_bytes())
}

pub(crate) fn update(sha: &mut Sha) -> Result<[u8; 32], Error> {
    let public_key = unsafe {
        log::info!("Sending beacon with key: {:02x?}", BEACON_PUBLIC_KEY);
        BEACON_COUNT += 1;
        if BEACON_COUNT >= BEACONS_BEFORE_ROLLOVER {
            log::debug!(
                "Beacon count {} for rollover reached.",
                BEACONS_BEFORE_ROLLOVER
            );
            rollover(sha)?
        } else {
            BEACON_PUBLIC_KEY
        }
    };
    Ok(public_key)
}

fn rollover(sha: &mut Sha) -> Result<[u8; 32], Error> {
    unsafe {
        log::info!("Doing rollover after {} beacons.", BEACON_COUNT);
    };
    let mut jayjeer: Jayjeer = match state::NutState::read_from_flash(sha) {
        Err(Error::InvalidFlashData) => {
            log::error!("Invalid checksum in flash data.");
            return Err(Error::InvalidFlashData);
        }
        Err(e) => return Err(e),
        Ok(state) => {
            log::info!("Loaded keys from flash.");
            (&state).into()
        }
    };

    jayjeer.rollover();

    log::debug!("Writing rolled-over key back to flash.");
    let mut state = state::NutState::from(&jayjeer);
    state.write_to_flash(sha);

    reset_beacon_data(&mut jayjeer);
    Ok(jayjeer.get_current_public_key().to_bytes())
}
