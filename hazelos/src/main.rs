#![no_std]
#![no_main]
#![feature(error_in_core)]

#[cfg(feature = "esp32")]
extern crate esp32_hal as hal;
#[cfg(feature = "esp32c3")]
extern crate esp32c3_hal as hal;

extern crate derive_more;

use esp_backtrace as _;

use hal::{
    clock::ClockControl,
    peripherals::Peripherals,
    prelude::*,
    reset::{software_reset, SleepSource},
    rng::Rng,
    rtc_cntl::{
        get_reset_reason, get_wakeup_cause,
        sleep::{RtcSleepConfig, TimerWakeupSource},
        SocResetReason,
    },
    sha::{Sha, ShaMode},
    Delay, Rtc,
};

use core::fmt::Debug;
use core::time::Duration;

mod beacon;
mod state;

#[cfg(feature = "ble")]
mod ble;

const OS_NAME: &str = env!("CARGO_PKG_NAME");
const OS_VER: &str = env!("CARGO_PKG_VERSION");
const OS_AUTHORS: &str = env!("CARGO_PKG_AUTHORS");
const OS_LICENSE: &str = env!("CARGO_PKG_LICENSE");
const OS_LINK: &str = env!("CARGO_PKG_REPOSITORY");

const PRODUCT_NAME: &str = "🐿️ SpotNut 1.0";

#[derive(Debug, derive_more::Display, derive_more::Error)]
enum Error {
    #[display("The ESP board failed to perform")]
    BoardError,
    #[display("The persistent data in flash memory was invalid")]
    InvalidFlashData,
}

fn log_banner() {
    log::info!("[STARTUP] {} ({} {})", PRODUCT_NAME, OS_NAME, OS_VER);
    log::info!("[STARTUP] {}", OS_AUTHORS);
    log::info!("[STARTUP] {}", OS_LICENSE);
    log::info!("[STARTUP] {}", OS_LINK);
}

#[entry]
fn main() -> ! {
    #[cfg(feature = "log")]
    esp_println::logger::init_logger_from_env();

    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();
    let mut delay = Delay::new(&clocks);
    let mut rtc = Rtc::new(peripherals.RTC_CNTL);
    let mut rng = Rng::new(peripherals.RNG);

    let reason = get_reset_reason(hal::Cpu::ProCpu).unwrap_or(SocResetReason::ChipPowerOn);
    let cause = get_wakeup_cause();
    let res = match (reason, cause) {
        (SocResetReason::ChipPowerOn, _) | (SocResetReason::CoreSw, _) => {
            log::debug!("Wakeup reason: Power on or reset");
            log_banner();
            beacon::init(&mut rng, &mut Sha::new(peripherals.SHA, ShaMode::SHA256))
        }
        (SocResetReason::CoreDeepSleep, SleepSource::Timer) => {
            log::debug!("Wakeup reason: RTC");
            beacon::update(&mut Sha::new(peripherals.SHA, ShaMode::SHA256))
        }
        (_, _) => {
            log::error!("Unexpected wakeup reason. Resetting device!");
            software_reset();
            unreachable!("Not reachable after reset");
        }
    };
    match res {
        Ok(public_key) => {
            #[cfg(feature = "ble")]
            {
                #[cfg(any(feature = "esp32c3"))]
                let time_peripheral = peripherals.SYSTIMER;
                #[cfg(any(feature = "esp32"))]
                let time_peripheral = peripherals.TIMG1;

                let ble_init = ble::init(
                    time_peripheral,
                    peripherals.BT,
                    rng,
                    system.radio_clock_control,
                    &clocks,
                    &public_key,
                );
                match ble_init {
                    Ok(_) => log::debug!("Initializing BLE was successful."),
                    Err(_) => {
                        log::error!("Could not initialize BLE. Resetting device!");
                        software_reset();
                        unreachable!("Not reachable after reset");
                    }
                };
            }

            // FIXME do we need to wake up for beacons?
            let beacon_secs: u64 = (rng.random()
                % (beacon::BEACON_SECS_MAX - beacon::BEACON_SECS_MIN) as u32
                + beacon::BEACON_SECS_MIN as u32)
                .into();
            log::debug!("Going into deep sleep for {} seconds.", beacon_secs);
            let mut sleep_config = RtcSleepConfig::deep();
            sleep_config.set_rtc_fastmem_pd_en(false);
            let wakeup_rollover = TimerWakeupSource::new(Duration::new(beacon_secs, 0));
            rtc.sleep(&sleep_config, &[&wakeup_rollover], &mut delay);
        }
        Err(_) => {
            log::error!("Error during operation. Resetting device.");
            software_reset();
        }
    };

    unreachable!("Not reachable after going to deep sleep or software reset");
}
