use hal::sha::Sha;

use nb::block;

use embedded_storage::{ReadStorage, Storage};
use esp_storage::FlashStorage;

use libjayjeer::Jayjeer;

use crate::Error;

const FLASH_OFFSET: u32 = 0x310000;

pub(crate) struct NutState {
    current_private_key: [u8; 32],
    peer_public_key: [u8; 32],
    checksum: [u8; 32],
}

impl NutState {
    pub(crate) fn read_from_flash(sha: &mut Sha) -> Result<Self, Error> {
        log::debug!(
            "Loading state data from flash address 0x{:x?}",
            FLASH_OFFSET
        );
        let mut flash = FlashStorage::new();
        let mut bytes = [0u8; 96];
        flash.read(FLASH_OFFSET, &mut bytes).unwrap();
        let state = Self::from(&bytes);

        state.validate_checksum(sha).and(Ok(state))
    }

    pub(crate) fn write_to_flash(&mut self, sha: &mut Sha) {
        log::debug!("Saving state data to flash address 0x{:x?}", FLASH_OFFSET);

        self.update_checksum(sha);

        let mut flash = FlashStorage::new();
        let bytes: [u8; 96] = (&(*self)).into();
        log::debug!("Writing bytes: {:02x?}", bytes);
        flash.write(FLASH_OFFSET, &bytes).unwrap();
        log::debug!("Done writing.");
    }

    fn calculate_checksum(&self, sha: &mut Sha) -> [u8; 32] {
        let mut output = [0u8; 32];

        block!(sha.update(&self.current_private_key)).unwrap();
        block!(sha.update(&self.peer_public_key)).unwrap();
        block!(sha.finish(output.as_mut_slice())).unwrap();

        output
    }

    fn update_checksum(&mut self, sha: &mut Sha) {
        log::debug!("Updating checksum for state data.");
        self.checksum = self.calculate_checksum(sha);
    }

    pub(crate) fn validate_checksum(&self, sha: &mut Sha) -> Result<(), Error> {
        match self.calculate_checksum(sha) == self.checksum {
            true => Ok(()),
            false => Err(Error::InvalidFlashData),
        }
    }
}

impl From<&[u8; 96]> for NutState {
    fn from(value: &[u8; 96]) -> Self {
        Self {
            current_private_key: value[0..32].try_into().unwrap(),
            peer_public_key: value[32..64].try_into().unwrap(),
            checksum: value[64..96].try_into().unwrap(),
        }
    }
}

impl From<&NutState> for [u8; 96] {
    fn from(value: &NutState) -> [u8; 96] {
        let mut bytes = [0u8; 96];
        bytes[0..32].copy_from_slice(&value.current_private_key);
        bytes[32..64].copy_from_slice(&value.peer_public_key);
        bytes[64..96].copy_from_slice(&value.checksum);
        bytes
    }
}

impl From<&Jayjeer> for NutState {
    fn from(value: &Jayjeer) -> Self {
        NutState {
            current_private_key: value.get_current_private_key().to_bytes(),
            peer_public_key: [0u8; 32],
            checksum: [0u8; 32],
        }
    }
}

impl From<&NutState> for Jayjeer {
    fn from(value: &NutState) -> Jayjeer {
        Jayjeer::from(value.current_private_key)
    }
}
