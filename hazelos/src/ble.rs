use esp_backtrace as _;

use bleps::{
    ad_structure::{
        create_advertising_data, AdStructure, BR_EDR_NOT_SUPPORTED, LE_GENERAL_DISCOVERABLE,
    },
    att::Uuid,
    attribute_server::{AttributeServer, NotificationData, WorkResult},
    gatt, Ble, HciConnector,
};
use esp_wifi::{ble::controller::BleConnector, initialize, EspWifiInitFor};

use hal::{clock::Clocks, peripherals::BT, system::RadioClockControl, Rng};

#[cfg(any(feature = "esp32c3"))]
use hal::systimer::{Alarm, Target};

use libspotnuts::ble::LegacyBLEAdData;

#[cfg(any(feature = "esp32c3"))]
type EspWifiTimer = Alarm<Target, 0>;
#[cfg(any(feature = "esp32"))]
type EspWifiTimer = hal::timer::Timer<hal::timer::Timer0<hal::peripherals::TIMG1>>;

#[cfg(any(feature = "esp32c3"))]
type EspWifiTimePeripheral = hal::peripherals::SYSTIMER;
#[cfg(any(feature = "esp32"))]
type EspWifiTimePeripheral = hal::peripherals::TIMG1;

use crate::Error;
use crate::PRODUCT_NAME;

fn get_timer(peripheral: EspWifiTimePeripheral, clocks: &Clocks) -> EspWifiTimer {
    #[cfg(any(feature = "esp32"))]
    let timer = hal::timer::TimerGroup::new(peripheral, &clocks).timer0;
    #[cfg(any(feature = "esp32c3"))]
    let timer = hal::systimer::SystemTimer::new(peripheral).alarm0;

    timer
}

pub(crate) fn init(
    time_peripheral: EspWifiTimePeripheral,
    mut bt_peripheral: BT,
    rng: Rng,
    rccntl: RadioClockControl,
    clocks: &Clocks,
    public_key: &[u8; 32],
) -> Result<(), Error> {
    log::debug!("Initializing BLE adapter.");

    let init = initialize(
        EspWifiInitFor::Ble,
        get_timer(time_peripheral, clocks),
        rng,
        rccntl,
        clocks,
    )
    .or(Err(Error::BoardError))?;

    let connector = BleConnector::new(&init, &mut bt_peripheral);
    let hci = HciConnector::new(connector, esp_wifi::current_millis);

    let mut ble = Ble::new(&hci);
    ble.init().or(Err(Error::BoardError))?;

    log::debug!("Configuring BLE advertisements.");

    // FIXME maybe use custom parameters
    ble.cmd_set_le_advertising_parameters()
        .or(Err(Error::BoardError))?;

    // FIXME need to set MAC here as well
    let ad_data: bleps::Data = LegacyBLEAdData::encode_public_key_bytes(public_key)
        .try_into()
        .or(Err(Error::BoardError))?;
    ble.cmd_set_le_advertising_data(ad_data)
        .or(Err(Error::BoardError))?;
    ble.cmd_set_le_advertise_enable(true)
        .or(Err(Error::BoardError))?;

    Ok(())
}
