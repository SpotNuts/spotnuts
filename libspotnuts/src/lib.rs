#![no_std]

#[cfg(feature = "std")]
extern crate std;

pub mod ble;

#[cfg(test)]
mod tests;
