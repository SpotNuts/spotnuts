//! Functions for conveying jayjeer information over Bluetooth Low Energy

#[cfg(not(feature = "std"))]
use core::{convert::TryFrom, result::Result};

#[cfg(feature = "bleps")]
use bleps;

/// Company ID sponsored by velocitux UG
const COMPANY_ID: u16 = 0x0D27;
/// Marker byte as required and assigned by velocitux UG
const MARKER_BYTE: u8 = 0x94;

/// Advertising data for Bluetooth 4.0 legacy advertisements
pub struct LegacyBLEAdData {
    /// Locally-administered MAC address
    /// The MAC address will encode part of the public key
    pub mac: [u8; 6],
    /// Manufacturer-specific data payload
    pub manufacturer_specific: [u8; 27],
    /// Company ID for manufacturer-specific data
    pub company_id: u16,
}

impl LegacyBLEAdData {
    /// Encode a 32-byte public key into legacy advertisement data
    ///
    /// The encoding will use the 6 bytes of the MAC address and
    /// the 26 available bytes of the manufacturer-specific data payload.
    pub fn encode_public_key_bytes(public_key: &[u8; 32]) -> Self {
        // FIXME maybe find better encoding or use custom service

        log::debug!("Encoding public key {:02x?} for legacy BLE ad.", public_key);

        // Split 32 bytes public key into 6 bytes MAC address and 26 bytes data
        let mut mac: [u8; 6] = [0; 6];
        let mut manufacturer_specific: [u8; 27] = [0; 27];

        manufacturer_specific[0] = MARKER_BYTE;
        manufacturer_specific[1..27].copy_from_slice(&public_key[0..26]);
        mac[1..6].copy_from_slice(&public_key[26..31]);
        // The 2nd lowest bit of a locally-managed MAC must be 1
        // Fortunately, the highest bit of the last byte of a Curve25519
        // key is always 0, so we move bit 2 to bit 8 and set bit 2 to 1
        mac[0] = (public_key[31] | ((public_key[31] & 2) << 6)) | 2;

        log::debug!(
            "Encoded into MAC {:02x?}, MSD {:02x?}",
            mac,
            manufacturer_specific
        );

        Self {
            mac,
            manufacturer_specific,
            company_id: COMPANY_ID,
        }
    }

    /// Decode advertisement data from MAC address and manufacturer-specific
    /// data payload.
    ///
    /// The company ID and marker byte will be ignored.
    pub fn decode_public_key_bytes(&self) -> [u8; 32] {
        log::debug!(
            "Decoding ad data MAC {:02x?}, MSD {:02x?}",
            self.mac,
            self.manufacturer_specific
        );

        let mut public_key: [u8; 32] = [0; 32];
        public_key[0..26].copy_from_slice(&self.manufacturer_specific[1..27]);
        public_key[26..31].copy_from_slice(&self.mac[1..6]);
        // Move highest bit back to the second lowest bit in the public key
        // X25519 public keys always have the highest bit set to 0
        public_key[31] = (self.mac[0] | ((self.mac[0] & 0x80) >> 6)) & !0x80;

        log::debug!("Decoded into public key {:02x?}", public_key);
        public_key
    }
}

#[cfg(feature = "bleps")]
impl TryFrom<LegacyBLEAdData> for bleps::Data {
    type Error = bleps::ad_structure::AdvertisementDataError;

    fn try_from(value: LegacyBLEAdData) -> Result<Self, Self::Error> {
        bleps::ad_structure::create_advertising_data(&[
            bleps::ad_structure::AdStructure::ManufacturerSpecificData {
                company_identifier: value.company_id,
                payload: &value.manufacturer_specific,
            },
        ])
    }
}
